<?php namespace Factuursnel\Base\Action;

use \Clearweb\Clearworks\Action\ScriptAction;

class PrintWindow extends ScriptAction
{
    public function init()
    {
        $this->addStyle('/invoices/css/print.css');
        return parent::init();
    }
    
	public function getActionScript()
	{
		return "window.print()";
	}

}
